package ru.terrakok.gitlabclient.di.module

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import ru.terrakok.gitlabclient.system.flow.FlowRouter
import toothpick.config.Module

/**
 * Created by Konstantin Tskhovrebov (aka @terrakok) on 03.09.18.
 */
class FlowNavigationModule(globalRouter: Router) : Module() {
    init {
        val cicerone = Cicerone.create(FlowRouter(globalRouter))
        bind(FlowRouter::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.getNavigatorHolder())
    }
}
